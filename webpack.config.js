const { resolve ,join } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
require('whatwg-fetch');

const config = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'react-hot-loader/patch',
    'webpack-hot-middleware/client?quiet=true',
    './main.js',
    './scss/main.scss',
  ],
  output: {
    filename: 'bundle.js',
    path: resolve(__dirname, 'public'),
    publicPath: '/',
  },
  context: resolve(__dirname, 'app/frontend'),
  // devServer: {
  //   historyApiFallback: true,
  //   hot: true,
  //   contentBase: resolve(__dirname, 'public'),
  //   publicPath: '/'
  // },
  module: {
    rules: [
      // {
      //   enforce: "pre",
      //   test: /\.js$/,
      //   exclude: /node_modules/,
      //   loader: "eslint-loader"
      // },
      {
        test: /\.js$/,
        loaders: [
          'babel-loader',
        ],
        exclude: /node_modules/,
        include: join(__dirname, 'app')
      },
       {test: /\.scss$/,
         loaders: ['style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader?sourceMap']},
      // {
      //   test: /\.scss$/,
      //   exclude: /node_modules/,
      //   use: ExtractTextPlugin.extract({
      //     fallback: 'style-loader',
      //     use: ['css-loader',{
      //         loader: 'sass-loader',
      //         query: {sourceMap: false,},},],
      //     publicPath: '../'}),},
      {test: /\.(png|jpg|gif)$/,
        use: [{
          loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'image/png',
              name: 'images/[name].[ext]',
            }
          }
        ],
      },
      {
        test: /\.eot(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]'
            }
          }
        ],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'application/font-woff',
              name: 'fonts/[name].[ext]',
            }
          }
        ],
      },
      {
        test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'application/octet-stream',
              name: 'fonts/[name].[ext]',
            }
          }
        ],
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'image/svg+xml',
              name: 'images/[name].[ext]',
            }
          }
        ],
      },
    ]
  },

  plugins: [

    new webpack.ProvidePlugin({
       // 'Promise': 'es6-promise',
       'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
   }),

//     new webpack.ProvidePlugin({
//   //  'Promise': 'es6-promise', // Thanks Aaron (https://gist.github.com/Couto/b29676dd1ab8714a818f#gistcomment-1584602)
//     'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
// }),
    // new webpack.LoaderOptionsPlugin({
    //   test: /\.js$/,
    //   options: {
    //     eslint: {
    //       configFile: resolve(__dirname, '.eslintrc'),
    //       cache: false,
    //     }
    //   },
    // }),
    new webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify('development'),
    WEBPACK: true
  }
}),
    new webpack.optimize.ModuleConcatenationPlugin(),
    // new ExtractTextPlugin({ filename: './styles/style.css', disable: false, allChunks: true }),
    new CopyWebpackPlugin([{ from: '../vendors', to: 'vendors' }]),
    new OpenBrowserPlugin({ url: 'http://localhost:8080' }),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: `${__dirname}/app/html/index.html`,
      filename: 'index.html',
      inject: 'body',
    })
  ],
};

module.exports = config;
