/* @flow */
import React from "react";
import { renderRoutes } from "react-router-config";

const AppRoot = (props) => {
  return (
  <React.Fragment>
      {renderRoutes(props.route.routes)}
  </React.Fragment>
  );
};

export default AppRoot;
