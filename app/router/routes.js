/* @flow */
import Home from "../frontend/components/pages/home.js";
import AppRoot from "./app-root";

const routes = [
  { component: AppRoot, routes: [{ path: "/", exact: true, component: Home }] }
];
export default routes;
