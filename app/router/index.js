/* @flow */
import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import routes from "./routes";
import Headers from "../frontend/components/modular/headers.js";
const Root = () => {
  return (
    <React.Fragment>
      <Headers />
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        {renderRoutes(routes)}
      </BrowserRouter>
    </React.Fragment>
  );
};

export default Root;
