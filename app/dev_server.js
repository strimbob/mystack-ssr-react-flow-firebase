import path from "path";
import express from "express";
import webpack from "webpack";
import { indexRequest } from "./expressFuc";
// import config from './redux/firebase/configFireBase.js'
const app = express();

if (process.env.NODE_ENV === "development") {
  const config = require("../webpack.config");
  const compiler = webpack(config);
  app.use(
    require("webpack-dev-middleware")(compiler, {
      noInfo: true,
      publicPath: config.output.publicPath,
      stats: {
        assets: false,
        colors: true,
        version: false,
        hash: false,
        timings: false,
        chunks: false,
        chunkModules: false
      }
    })
  );
  app.use(require("webpack-hot-middleware")(compiler));
  app.use(express.static(path.resolve(__dirname, "app")));
  app.use(express.static(path.resolve(__dirname, "../public")));
} else if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.resolve(__dirname, "../public")));
  // app.get('/**/**/**/**',fisheryRequest);
}
// app.post('/joinUp',joinUpRequest);
// app.post('/sendContactEmail',sendContactEmail);
app.get("*", indexRequest);

app.listen(8080, "0.0.0.0", err => {
  if (err) {
    console.error(err);
  } else {
    console.log("Using the " + process.env.NODE_ENV + " version");
    console.info("Listening at http://localhost:8080");
  }
});
