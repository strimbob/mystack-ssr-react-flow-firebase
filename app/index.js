/* @flow */
import path from "path";
import express from "express";
import * as functions from "firebase-functions";
const firebase = require("firebase-admin");
import { indexRequest } from "./expressFuc";
const app = express();
const assets = express.static(path.join(__dirname, "../public"));
app.use(assets);
app.get("*", indexRequest);
export let daBrain = functions.https.onRequest(app);
