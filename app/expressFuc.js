/* @flow */
import React from "react";
import { renderToString } from "react-dom/server";
import StaticRouter from "react-router-dom/StaticRouter";
import { renderRoutes } from "react-router-config";
import express from "express";
import fs from "fs";
import path from "path";
import routes from "./router/routes.js";
import { Provider } from "react-redux";
import store from "./redux/1_store/store.js";
import { mobileParser } from "react-responsive-redux";
import { Helmet } from "react-helmet";
import config from "./redux/firebase/configFireBase.js";
const { dispatch } = store;
const live = false;
const app = express();
const _index = fs.readFileSync(
  path.resolve(__dirname, "./public", "nerves.html"),
  "utf8"
);
let renderdPages = [{}];
renderdPages.length = 0;
const oneDay = 86400000;
//____________________________________________________
function renderFullPage(html, preloadedState, helmet) {
  let htmlFinal = _index.replace("<!--- ::APP::----->", html);
  htmlFinal = htmlFinal.replace(
    "<!--- ::head__html::----->",
    helmet.htmlAttributes.toString()
  );
  htmlFinal = htmlFinal.replace(
    "<!--- ::head__title::----->",
    helmet.title.toString()
  );
  htmlFinal = htmlFinal.replace(
    "<!--- ::head__link::----->",
    helmet.link.toString()
  );
  htmlFinal = htmlFinal.replace(
    "<!--- ::head__meta::----->",
    helmet.meta.toString()
  );
  htmlFinal = htmlFinal.replace(
    "<!--- ::head__noscript::----->",
    helmet.noscript.toString()
  );
  let store_init = `<script> window.__PRELOADED_STATE__ = ${JSON.stringify(
    preloadedState
  ).replace(/</g, "\\u003c")} </script>`;
  htmlFinal = htmlFinal.replace("<!--- ::store::----->", store_init);
  return htmlFinal;
}
//____________________________________________________
export const indexRequest = (request: Object, response: Object) => {
  if (process.env.NODE_ENV === "development") {
    response.send(_index);
  } else {
    response.send(_index);
  }
};
//____________________________________________________
const renderPage = (request: Object, response: Object) => {
  let context = {};
  const mobileDetect = mobileParser(request);
  const content = renderToString(
    <Provider store={store}>
      <div>
        <StaticRouter location={request.url} context={context}>
          {renderRoutes(routes)}
        </StaticRouter>
      </div>
    </Provider>
  );
  const helmet = Helmet.renderStatic();
  const preloadedState = store.getState();
  const newRenderPage = renderFullPage(content, preloadedState, helmet);
  response.send(newRenderPage);
};
