 /* @flow */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import store from '../redux/1_store/store.js';
import Root from '../router/index';
import config from '../redux/firebase/configFireBase.js'
// import { applyMiddleware, createStore } from "redux"
// import { createLogger } from 'redux-logger'
// import thunk from "redux-thunk"
// import promise from "redux-promise-middleware"
// import reducer from "../redux/3_reducers"

const preloadedState = window.__PRELOADED_STATE__

delete window.__PRELOADED_STATE__

// const middleware = applyMiddleware(promise(), thunk, createLogger())
// const store = createStore(reducer, preloadedState,middleware)

const render = (Component) => {
  ReactDOM.render(
        <Provider store={store}>
          <AppContainer>
            <Component />
          </AppContainer>
        </Provider>,
    document.getElementById('root'),
  );
};

render(Root);

if (module.hot) {
  module.hot.accept('../router/index', () => {
    const newApp = require('../router/index').default;
    render(newApp);
  });
}
