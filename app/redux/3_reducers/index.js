import { combineReducers } from "redux";
import mqtt_reducer from "./mqtt_reducer";
export default combineReducers({
  mqtt_reducer
});
