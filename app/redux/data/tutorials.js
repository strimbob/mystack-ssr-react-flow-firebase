export const tutorials = {
title : "tutorials",
body: "You can understand how a track will sound just by looking at it, using your visual cortex to help you make music.  You can see combinations of sound and rhythms in iris that would be impossible to understand in timeline based pieces of software.",
link: "nnIaLjP_wIU",
img: "/static/svg/logowithtext.svg",
playlist: [
  {link: "Bc5wzPFzwXk",
  title: "how to set up Iris with ableton live"
  },

  {link: "A_AiPtOA3gs",
  title: "Beat drawing the basics "
  },

  {link: "N741qUZizjQ",
  title: "How to control Iris"
  },

  {link: "TbzOo45wNG4",
  title: "Using the launchPad"
  },

  {link: "jnDRoCSFXTQ",
  title: "Recording into ableton live"
  },

  {link: "25HnTCrIrlo",
  title: "Synth control in depth"
  },


]
}
