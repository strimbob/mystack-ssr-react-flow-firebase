/* @flow */
import firebase from "firebase";

export const put = (path: String, data: Object) => {
  firebase
    .database()
    .ref(path)
    .push(data);
};

export const getChild = parent => {
  const ref = firebase.database().ref(parent);
  return ref.once("value");
};
