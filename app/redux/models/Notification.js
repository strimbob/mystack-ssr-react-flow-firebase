//@flow
import { Record } from "immutable";

export interface NotificationInterface {
  setType(_type: string): Notification;
  setIsDocked(_docked: boolean): Notification;
  setTemperature(_temperature: number): Notification;
  setAltitude(_altitude: number): Notification;
  setTimerStart(_time: number): Notification;
  setIsMaintenance(_maintenance: boolean): Notification;
  setAction(_action: string): Notification;
  setMessageToUser(_message: string): Notification;
  getId(): string;
  getType(): string;
  getIsDocked(): boolean;
  getTemperature(): number;
  getAltitude(): number;
  getTimerStart(): number;
  getIsMaintenance(): boolean;
  getAction(): string;
  getMessageToUser(): string;
  getPath(): string;
  setTimeDelta(timeDelta: number): Notification;
  getTimeDelta(): number;
}

export type NotificationType = {
  id: string,
  type: string,
  docked: boolean,
  temperature: number,
  altitude: number,
  timer: number,
  maintenance: boolean,
  action: string,
  MessageToUser: string,
  path: string,
  timeDelta: number
};
const defaultNotification: NotificationType = {
  id: "DL3_1217",
  type: "",
  docked: false,
  temperature: 0,
  altitude: 60,
  timer: 0,
  maintenance: true,
  action: "Needs maintenances",
  MessageToUser: "",
  timeDelta: 0,
  path: "repair.svg"
};

const NotificationRecord = Record(defaultNotification);

export class Notification extends NotificationRecord<NotificationType>
  implements NotificationInterface {
  constructor(
    id: string,
    type: string,
    docked: boolean,
    temperature: number,
    altitude: number,
    timer: number,
    maintenance: boolean,
    action: string,
    MessageToUser: string,
    path: string,
    timeDelta: number
  ) {
    super({
      id,
      type,
      docked,
      temperature,
      altitude,
      timer,
      maintenance,
      action,
      MessageToUser,
      path,
      timeDelta
    });
  }

  setTimeDelta(timeDelta: number) {
    return this.set("timeDelta", timeDelta);
  }
  getTimeDelta() {
    return this.get("timeDelta");
  }
  setType(_type: string): Notification {
    return this.set("type", _type);
  }
  setIsDocked(_docked: boolean): Notification {
    return this.set("docked", _docked);
  }
  setTemperature(_temperature: number): Notification {
    return this.set("temperature", _temperature);
  }
  setAltitude(_altitude: number): Notification {
    return this.set("altitude", _altitude);
  }
  setTimerStart(_time: number): Notification {
    return this.set("timer", _time);
  }
  setIsMaintenance(_maintenance: boolean): Notification {
    return this.set("maintenance", _maintenance);
  }
  setAction(_action: string): Notification {
    return this.set("action", _action);
  }
  setMessageToUser(_message: string): Notification {
    return this.set("MessageToUser", _message);
  }

  getId(): string {
    return this.get("id");
  }
  getType(): string {
    return this.get("type");
  }
  getIsDocked(): boolean {
    return this.get("docked");
  }
  getTemperature(): number {
    return this.get("temperature");
  }
  getAltitude(): number {
    return this.get("altitude");
  }
  getTimerStart(): number {
    return this.get("timer");
  }
  getIsMaintenance(): boolean {
    return this.get("maintenance");
  }
  getAction(): string {
    return this.get("action");
  }
  getMessageToUser(): string {
    return this.get("MessageToUser");
  }

  getPath(): string {
    return this.get("path");
  }
}
