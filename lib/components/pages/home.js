'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require('react');

var React = _interopRequireWildcard(_react);

var _reactResponsiveRedux = require('react-responsive-redux');

var _reactRedux = require('react-redux');

var _indexPage = require('../../../redux/data/indexPage.js');

var _remember = require('../../../redux/2_action/remember.js');

var _props_type = require('../../types/props_type.js');

var _fisheryPage = require('../../../redux/2_action/fisheryPage.js');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var yes = null;
var IndexMain = (_dec = (0, _reactRedux.connect)(function (store) {
  return {
    remember: store.remember,
    fishery: store.fishery
  };
}), _dec(_class = function (_React$Component) {
  _inherits(IndexMain, _React$Component);

  function IndexMain(props) {
    _classCallCheck(this, IndexMain);

    return _possibleConstructorReturn(this, (IndexMain.__proto__ || Object.getPrototypeOf(IndexMain)).call(this, props));
  }

  _createClass(IndexMain, [{
    key: 'componentWillMount',
    value: function componentWillMount() {

      if (this.props.fishery.fetched) {
        console.log("this.props.fishery");
        console.log(this.props.fishery);
        yes = this.props.fishery.story.fishery;
      }

      // if(window.__fisheryPage__){
      //     console.log("window fihser")
      // }else{
      //   console.log("else fihser")
      // }

      //    this.props.dispatch(getFisheryPage("/data/"+this.props.location.pathname))
      //    //
      // this.props.dispatch(remember("hello"))
      //       console.log(this.props.fishery)
    }
  }, {
    key: 'render',
    value: function render() {

      if (this.props.fishery.fetched) {
        return React.createElement(
          'div',
          null,
          React.createElement(
            'h1',
            null,
            this.props.fishery.story.fishery,
            ' '
          ),
          ' '
        );
      } else {
        return React.createElement(
          React.Fragment,
          null,
          React.createElement(
            'h1',
            null,
            ' bens stack'
          ),
          React.createElement(
            _reactResponsiveRedux.PhoneScreen,
            null,
            React.createElement(
              'div',
              null,
              React.createElement(
                'h1',
                null,
                "You are a PhoneScreen device hi it is STILL fgfg  asd working! now way and t fast!" + this.props.remember.rembered
              )
            )
          ),
          React.createElement(
            _reactResponsiveRedux.TabletScreen,
            null,
            React.createElement(
              'div',
              null,
              React.createElement(
                'h1',
                null,
                " You are a TabletScreen device" + this.props.remember.rembered,
                ' '
              )
            )
          ),
          React.createElement(
            _reactResponsiveRedux.DesktopScreen,
            null,
            React.createElement(
              'div',
              null,
              React.createElement(
                'h1',
                null,
                "You are a DesktopScreen device" + this.props.remember.rembered
              )
            )
          )
        );
      }
    }
  }]);

  return IndexMain;
}(React.Component)) || _class);
exports.default = IndexMain;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(yes, 'yes', 'app/frontend/components/pages/home.js');

  __REACT_HOT_LOADER__.register(IndexMain, 'IndexMain', 'app/frontend/components/pages/home.js');
}();

;