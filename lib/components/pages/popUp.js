"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require("react-redux");

var _popUp = require("../organisms/popUps/popUp.js");

var _popUp2 = _interopRequireDefault(_popUp);

var _contact = require("../../../redux/2_action/contact.js");

var _cookies = require("../../../redux/2_action/cookies.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PopUpPages = (_dec = (0, _reactRedux.connect)(function (store) {
  return {
    contactForm: store.contactForm
  };
}), _dec(_class = function (_React$Component) {
  _inherits(PopUpPages, _React$Component);

  function PopUpPages() {
    _classCallCheck(this, PopUpPages);

    return _possibleConstructorReturn(this, (PopUpPages.__proto__ || Object.getPrototypeOf(PopUpPages)).apply(this, arguments));
  }

  _createClass(PopUpPages, [{
    key: "clickHanlder",
    value: function clickHanlder(action) {
      if (action == "close") {
        this.props.dispatch((0, _contact.popUpClose)());
      }
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      console.log("start cookie timer");
      this.props.dispatch((0, _cookies.firstLoadCookie)());
    }
  }, {
    key: "render",
    value: function render() {
      if (this.props.contactForm.popUpOpen) {
        return _react2.default.createElement(
          "div",
          { className: "popup" },
          _react2.default.createElement(
            "div",
            { className: "popup_inner" },
            _react2.default.createElement("div", { onClick: this.clickHanlder.bind(this, "close"), className: "mm-popup__close" }),
            _react2.default.createElement(
              "div",
              { className: "popup__title" },
              " Join Us "
            ),
            _react2.default.createElement(_popUp2.default, null)
          )
        );
      } else {
        return null;
      }
    }
  }]);

  return PopUpPages;
}(_react2.default.Component)) || _class);
exports.default = PopUpPages;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(PopUpPages, "PopUpPages", "app/frontend/components/pages/popUp.js");
}();

;