'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactRedux = require('react-redux');

var _reactHotLoader = require('react-hot-loader');

var _store = require('../redux/1_store/store.js');

var _store2 = _interopRequireDefault(_store);

var _index = require('../router/index');

var _index2 = _interopRequireDefault(_index);

var _configFireBase = require('../redux/firebase/configFireBase.js');

var _configFireBase2 = _interopRequireDefault(_configFireBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import { applyMiddleware, createStore } from "redux"
// import { createLogger } from 'redux-logger'
// import thunk from "redux-thunk"
// import promise from "redux-promise-middleware"
// import reducer from "../redux/3_reducers"

var preloadedState = window.__PRELOADED_STATE__;


delete window.__PRELOADED_STATE__;

// const middleware = applyMiddleware(promise(), thunk, createLogger())
// const store = createStore(reducer, preloadedState,middleware)

var render = function render(Component) {
  _reactDom2.default.render(_react2.default.createElement(
    _reactRedux.Provider,
    { store: _store2.default },
    _react2.default.createElement(
      _reactHotLoader.AppContainer,
      null,
      _react2.default.createElement(Component, null)
    )
  ), document.getElementById('root'));
};

render(_index2.default);

if (module.hot) {
  module.hot.accept('../router/index', function () {
    var newApp = require('../router/index').default;
    render(newApp);
  });
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(preloadedState, 'preloadedState', 'app/frontend/main.js');

  __REACT_HOT_LOADER__.register(render, 'render', 'app/frontend/main.js');
}();

;