'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _reactMaterialUiFormValidator = require('react-material-ui-form-validator');

var _contact = require('../../../redux/2_action/contact.js');

var _reactRedux = require('react-redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SimpleFormExample = (_dec = (0, _reactRedux.connect)(function (store) {
    return {};
}), _dec(_class = function (_React$Component) {
    _inherits(SimpleFormExample, _React$Component);

    function SimpleFormExample(props) {
        _classCallCheck(this, SimpleFormExample);

        var _this = _possibleConstructorReturn(this, (SimpleFormExample.__proto__ || Object.getPrototypeOf(SimpleFormExample)).call(this, props));

        _this.state = {
            formData: {
                email: '',
                password: ''
            },
            submitted: false
        };

        _this.handleChange = _this.handleChange.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        return _this;
    }

    _createClass(SimpleFormExample, [{
        key: 'handleChange',
        value: function handleChange(event) {
            var formData = this.state.formData;

            formData[event.target.name] = event.target.value;
            this.setState({ formData: formData });
        }
    }, {
        key: 'handleSubmit',
        value: function handleSubmit() {
            var _this2 = this;

            this.setState({ submitted: true }, function () {
                setTimeout(function () {
                    return _this2.setState({ submitted: false });
                }, 5000);
            });
            this.props.dispatch((0, _contact.downloadRequst)(this.state.formData));
            this.props.history.push("/Download-free-midi-software");
        }
    }, {
        key: 'render',
        value: function render() {
            var _state = this.state,
                formData = _state.formData,
                submitted = _state.submitted;


            var button = _react2.default.createElement(_RaisedButton2.default, {
                className: "download__button download__button--confirm",
                type: 'submit',
                label: submitted && 'thank you for downloading' || !submitted && 'sign up - download',
                disabled: submitted,
                href: this.state.submitted ? "./t.dmg" : ""
            });

            // const { errorMessages, validators, requiredError, value, ...rest } = this.props;
            // console.log(ValidatorForm.validators)
            return _react2.default.createElement(
                _reactMaterialUiFormValidator.ValidatorForm,
                {
                    ref: 'form',
                    onSubmit: this.handleSubmit },
                _react2.default.createElement(_reactMaterialUiFormValidator.TextValidator, {
                    className: 'formPOPUP',
                    floatingLabelText: 'name',
                    onChange: this.handleChange,
                    name: 'password',
                    value: formData.password,
                    validators: ['required'],
                    errorMessages: ['your name']
                }),
                _react2.default.createElement('br', null),
                _react2.default.createElement(_reactMaterialUiFormValidator.TextValidator, {
                    className: 'formPOPUP',
                    floatingLabelText: 'Email',
                    onChange: this.handleChange,
                    name: 'email',
                    value: formData.email,
                    validators: ['required', 'isEmail'],
                    errorMessages: ['what is your email?', 'email is not valid']
                }),
                _react2.default.createElement('br', null),
                _react2.default.createElement(
                    'div',
                    { className: "download" },
                    _react2.default.createElement(
                        'div',
                        { className: "download__buttonSvg" },
                        button
                    )
                )
            );
        }
    }]);

    return SimpleFormExample;
}(_react2.default.Component)) || _class);

// <a href={__dirname+"./t.dmg"}>fdsfsdf </a>
// <button onclick='myUrlSaveAs("http://www.example.com/path/to/file.jpg")'>Save As</button>

exports.default = SimpleFormExample;
;

var _temp = function () {
    if (typeof __REACT_HOT_LOADER__ === 'undefined') {
        return;
    }

    __REACT_HOT_LOADER__.register(SimpleFormExample, 'SimpleFormExample', 'app/frontend/components/organisms/forms/donationFormTest.js');
}();

;