"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var initialState = {
  story: null,
  loadedStory: null,
  fetching: false,
  fetched: false,
  error: false
};

var remember = function remember() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments[1];

  switch (action.type) {
    case "NEW_PAGE_LOAD":
      {
        return _extends({}, state, { fetched: false, fetching: false });
      }
    case "ALL_READY_GOT":
      {
        return _extends({}, state, { fetched: true });
      }
    case "FETCHING_PAGE_FISHEY":
      {
        return _extends({}, state, { fetching: true });
      }
    case "FETCHED_PAGED_FISHEY":
      {
        return _extends({}, state, { fetching: false, fetched: true, story: action.payload });
      }
    case "LOADED_FISHERY_IS":
      {
        return _extends({}, state, { loadedStory: action.payload });
      }
  }
  return state;
};
var _default = remember;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(initialState, "initialState", "app/redux/3_reducers/fishery_red.js");

  __REACT_HOT_LOADER__.register(remember, "remember", "app/redux/3_reducers/fishery_red.js");

  __REACT_HOT_LOADER__.register(_default, "default", "app/redux/3_reducers/fishery_red.js");
}();

;