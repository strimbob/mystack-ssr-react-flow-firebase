"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  rembered: "string",
  fetching: false,
  fetched: false,
  error: false
};

var remember = function remember() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments[1];

  switch (action.type) {
    case "REMBER_HOW_TO_USE":
      {
        var _extends2;

        return _extends({}, state, (_extends2 = { fetching: true }, _defineProperty(_extends2, "fetching", false), _defineProperty(_extends2, "rembered", action.payload), _extends2));
      }
    case "USING_IT":
      {
        var _extends3;

        return _extends({}, state, (_extends3 = { fetching: true }, _defineProperty(_extends3, "fetching", false), _defineProperty(_extends3, "rembered", action.payload), _extends3));
      }
    case "FUCKYRD":
      {
        return _extends({}, state, { fetching: true, rembered: action.payload });
      }
  }
  return state;
};
var _default = remember;
exports.default = _default;
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(initialState, "initialState", "app/redux/3_reducers/remember_red.js");

  __REACT_HOT_LOADER__.register(remember, "remember", "app/redux/3_reducers/remember_red.js");

  __REACT_HOT_LOADER__.register(_default, "default", "app/redux/3_reducers/remember_red.js");
}();

;