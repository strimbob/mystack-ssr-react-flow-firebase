"use strict";

var _firebase = require("firebase");

var _firebase2 = _interopRequireDefault(_firebase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Test
// Initialize Firebase
var config = {
  apiKey: "AIzaSyAGsQslN-HNv0390DsTXL3cqdh6t_8b1aA",
  authDomain: "test-f600a.firebaseapp.com",
  databaseURL: "https://test-f600a.firebaseio.com",
  projectId: "test-f600a",
  storageBucket: "test-f600a.appspot.com",
  messagingSenderId: "233717720716"
};
_firebase2.default.initializeApp(config);
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(config, "config", "app/redux/firebase/configFireBase.js");
}();

;