"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Notification = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _immutable = require("immutable");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaultNotification = {
  id: "DL3_1217",
  type: "",
  docked: false,
  temperature: 0,
  altitude: 60,
  timer: 0,
  maintenance: true,
  action: "Needs maintenances",
  MessageToUser: "",
  timeDelta: 0,
  path: "repair.svg"
};

var NotificationRecord = (0, _immutable.Record)(defaultNotification);

var Notification = exports.Notification = function (_NotificationRecord) {
  _inherits(Notification, _NotificationRecord);

  function Notification(id, type, docked, temperature, altitude, timer, maintenance, action, MessageToUser, path, timeDelta) {
    _classCallCheck(this, Notification);

    return _possibleConstructorReturn(this, (Notification.__proto__ || Object.getPrototypeOf(Notification)).call(this, {
      id: id,
      type: type,
      docked: docked,
      temperature: temperature,
      altitude: altitude,
      timer: timer,
      maintenance: maintenance,
      action: action,
      MessageToUser: MessageToUser,
      path: path,
      timeDelta: timeDelta
    }));
  }

  _createClass(Notification, [{
    key: "setTimeDelta",
    value: function setTimeDelta(timeDelta) {
      return this.set("timeDelta", timeDelta);
    }
  }, {
    key: "getTimeDelta",
    value: function getTimeDelta() {
      return this.get("timeDelta");
    }
  }, {
    key: "setType",
    value: function setType(_type) {
      return this.set("type", _type);
    }
  }, {
    key: "setIsDocked",
    value: function setIsDocked(_docked) {
      return this.set("docked", _docked);
    }
  }, {
    key: "setTemperature",
    value: function setTemperature(_temperature) {
      return this.set("temperature", _temperature);
    }
  }, {
    key: "setAltitude",
    value: function setAltitude(_altitude) {
      return this.set("altitude", _altitude);
    }
  }, {
    key: "setTimerStart",
    value: function setTimerStart(_time) {
      return this.set("timer", _time);
    }
  }, {
    key: "setIsMaintenance",
    value: function setIsMaintenance(_maintenance) {
      return this.set("maintenance", _maintenance);
    }
  }, {
    key: "setAction",
    value: function setAction(_action) {
      return this.set("action", _action);
    }
  }, {
    key: "setMessageToUser",
    value: function setMessageToUser(_message) {
      return this.set("MessageToUser", _message);
    }
  }, {
    key: "getId",
    value: function getId() {
      return this.get("id");
    }
  }, {
    key: "getType",
    value: function getType() {
      return this.get("type");
    }
  }, {
    key: "getIsDocked",
    value: function getIsDocked() {
      return this.get("docked");
    }
  }, {
    key: "getTemperature",
    value: function getTemperature() {
      return this.get("temperature");
    }
  }, {
    key: "getAltitude",
    value: function getAltitude() {
      return this.get("altitude");
    }
  }, {
    key: "getTimerStart",
    value: function getTimerStart() {
      return this.get("timer");
    }
  }, {
    key: "getIsMaintenance",
    value: function getIsMaintenance() {
      return this.get("maintenance");
    }
  }, {
    key: "getAction",
    value: function getAction() {
      return this.get("action");
    }
  }, {
    key: "getMessageToUser",
    value: function getMessageToUser() {
      return this.get("MessageToUser");
    }
  }, {
    key: "getPath",
    value: function getPath() {
      return this.get("path");
    }
  }]);

  return Notification;
}(NotificationRecord);

;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(defaultNotification, "defaultNotification", "app/redux/models/Notification.js");

  __REACT_HOT_LOADER__.register(NotificationRecord, "NotificationRecord", "app/redux/models/Notification.js");

  __REACT_HOT_LOADER__.register(Notification, "Notification", "app/redux/models/Notification.js");
}();

;