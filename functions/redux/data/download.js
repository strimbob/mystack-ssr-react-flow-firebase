"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var download = exports.download = {
  title: "Create muisc with iris",
  subtile: "mac osx",
  button: "DOWNLOAD IRIS",
  body: "Iris frees you from the constraints of a timeline, enables you to jam, play, create structures on the fly in a repeatable way.",
  link: "../static/svg/danceWebreadyCom.svg"

};
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(download, "download", "app/redux/data/download.js");
}();

;