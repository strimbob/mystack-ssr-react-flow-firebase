"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var indexPage = exports.indexPage = [{
  title: "A VISUAL SEQUENCER",
  body: "The first nodal software sequencer made as a tool for producers. Plug in to Ableton Live , Logic , Cubase or anything else that uses Midi. By imagining how  a visual sequencer should work, we re-thought how time can work.  Iris was the result",
  link: ""
}, {
  title: "Iris is a nonlinear sequencer ",
  body: "Find   infinite variations in your loops, tween  between them, save the tweens, build a track from these variations easily. Fast and fun.  This nonlinear way of working let  you focus   on the structure of a track. Editing the structure has never been easier.",
  link: "zFA6hup8Uzk"
}, {
  title: "HIGHLY CONTROLLABLE  LIVE PERFORMANCE TOOL ",
  body: "",
  link: "2kmlR_NFZKU"
}];
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(indexPage, "indexPage", "app/redux/data/indexPage.js");
}();

;