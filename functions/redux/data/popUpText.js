"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var popUp_Text = exports.popUp_Text = {
  mainText: "Iris is adding new features regularly. Be part of the journey and don't miss out",
  thankYou: "we will keep you dated",
  messageSent: "Thank you for caring"
};
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(popUp_Text, "popUp_Text", "app/redux/data/popUpText.js");
}();

;