"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.newpageLoad = newpageLoad;
exports.gotFromReduxMemory = gotFromReduxMemory;
exports.getFisheryPage = getFisheryPage;
exports.setLoadedStory = setLoadedStory;
exports.ssrGetFact = ssrGetFact;

var _fireBaseReqest = require("../firebase/fireBaseReqest.js");

function newpageLoad() {
  return function (dispatch) {
    dispatch({ type: "NEW_PAGE_LOAD" });
  };
}

function gotFromReduxMemory() {
  return function (dispatch) {
    dispatch({ type: "ALL_READY_GOT" });
  };
}

function getFisheryPage(path) {
  var _path = "data/England/Yorkshire/River Ure/Bolton Estate";
  return function (dispatch) {
    dispatch({ type: "FETCHING_PAGE_FISHEY" });
    (0, _fireBaseReqest.getStoryFireBase)(_path).then(function (_story) {
      setLoadedStory(path);
      dispatch({ type: "FETCHED_PAGED_FISHEY", payload: _story });
    });
  };
}

function setLoadedStory(path) {
  return function (dispatch) {
    dispatch({ type: "LOADED_FISHERY_IS", payload: path });
  };
}

function ssrGetFact(_story) {
  return function (dispatch) {
    dispatch({ type: "FETCHING_PAGE_FISHEY" });
    dispatch({ type: "FETCHED_PAGED_FISHEY", payload: _story });
  };
}
;

var _temp = function () {
  if (typeof __REACT_HOT_LOADER__ === 'undefined') {
    return;
  }

  __REACT_HOT_LOADER__.register(newpageLoad, "newpageLoad", "app/redux/2_action/fisheryPage.js");

  __REACT_HOT_LOADER__.register(gotFromReduxMemory, "gotFromReduxMemory", "app/redux/2_action/fisheryPage.js");

  __REACT_HOT_LOADER__.register(getFisheryPage, "getFisheryPage", "app/redux/2_action/fisheryPage.js");

  __REACT_HOT_LOADER__.register(setLoadedStory, "setLoadedStory", "app/redux/2_action/fisheryPage.js");

  __REACT_HOT_LOADER__.register(ssrGetFact, "ssrGetFact", "app/redux/2_action/fisheryPage.js");
}();

;