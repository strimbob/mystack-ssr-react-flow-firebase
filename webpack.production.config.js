const { resolve } = require('path');
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const HtmlCriticalPlugin = require("html-critical-webpack-plugin");

const config = {
  devtool: 'cheap-module-source-map',
  entry: [
    './main.js',
    './scss/main.scss'
  ],
  context: resolve(__dirname, 'app/frontend'),
  output: {
    filename: 'bundle.js',
    path: resolve(__dirname, 'public'),
    publicPath: '/',
  },

  plugins: [
    new webpack.ProvidePlugin({
       // 'Promise': 'es6-promise',
       'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'
   }),
//     new webpack.ProvidePlugin({
//     'Promise': 'es6-promise', // Thanks Aaron (https://gist.github.com/Couto/b29676dd1ab8714a818f#gistcomment-1584602)
//     'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
// }),
    new FaviconsWebpackPlugin(`${__dirname}/app/logo/logo.png`),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new HtmlWebpackPlugin({
      template: `${__dirname}/app/html/index.html`,
      filename: `${__dirname}/public/indexother.html`,
      inject: 'body',
    }
  ),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new webpack.optimize.UglifyJsPlugin({
      beautify: false
    }),
    new webpack.DefinePlugin({ 'process.env': { NODE_ENV: JSON.stringify('production') } }),
    new ExtractTextPlugin({ filename: './styles/style.css', disable: false, allChunks: true }),
    new CopyWebpackPlugin([{ from: '../vendors', to: 'vendors' }]),
    new HtmlCriticalPlugin({
      base: path.join(path.resolve(__dirname), 'public/'),
      src: 'indexother.html',
      dest: 'indexother.html',
      inline: true,
      minify: true,
      extract: false,
      width: 375,
      height: 565,
      penthouse: {
        blockJSRequests: false,
      }
}),
// new workboxPlugin({
// globDirectory: "public",
// globPatterns: ['**/*.{html,js,css,json,png,jpg,webp}'],
// swSrc: `./app/serviceWorker/sw.js`,
// swDest: path.join("public", 'sw.js')
// }),
// new WebpackPwaManifest({
//   name: 'My Progressive Web App',
//   short_name: 'MyPWA',
//   description: 'My awesome Progressive Web App!',
//   background_color: '#00ff00',
//   inject: true,
//   icons: [
//     {
//       src: path.resolve(__dirname, 'app/logo/logo.jpg'),
//       sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
//     },
//     {
//       src: path.resolve(__dirname, 'app/logo/logo.jpg'),
//       size: '1024x1024' // you can also use the specifications pattern
//     }
//   ]
// })
  ],

  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
       // {test: /\.scss$/, loader: 'style-loader!css-loader!sass-loader'},
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            'css-loader','resolve-url-loader',
            { loader: 'sass-loader', query: { sourceMap: true } },
          ],
          publicPath: '../'
        }),
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'image/png',
              name: 'images/[name].[ext]',
            }
          }
        ],
      },
      {
        test: /\.eot(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'fonts/[name].[ext]'
            }
          }
        ],
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'application/font-woff',
              name: 'fonts/[name].[ext]',
            }
          }
        ],
      },
      {
        test: /\.[ot]tf(\?v=\d+.\d+.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'application/octet-stream',
              name: 'fonts/[name].[ext]',
            }
          }
        ],
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              mimetype: 'image/svg+xml',
              name: 'images/[name].[ext]',
            }
          }
        ],
      },
    ]
  },
};
module.exports = config;
